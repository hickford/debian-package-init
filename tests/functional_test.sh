#!/bin/bash
# A trivial functional test

set -eu

rm -rf functest.tmp
mkdir functest.tmp
cd functest.tmp

# C
../debpin.py --noask https://github.com/icholy/ttygif

# Python, GitHub
../debpin.py --noask https://github.com/asn-d6/onionbalance

# js
../debpin.py --noask https://github.com/milligram/milligram

# CSS
../debpin.py --noask https://github.com/dhg/Skeleton --pkg-name skeleton

# Python, GitLab
../debpin.py https://gitlab.com/jeancf/twoot

# Python, PyPI
../debpin.py https://pypi.org/project/arrow
