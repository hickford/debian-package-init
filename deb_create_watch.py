#!/usr/bin/env python3

"""
Create a debian/watch file for common software forge services.

Can be invoked directly or used through debpin.py

Examples:
    ./deb_create_watch.py https://gitlab.com/jeancf/twoot
    ./deb_create_watch.py https://github.com/bottlepy/bottle
    ./deb_create_watch.py https://pypi.org/project/arrow
"""

# See tests/functional_test.sh for basic functional tests


from argparse import ArgumentParser, RawDescriptionHelpFormatter
import logging
import re

log = logging.getLogger(__name__)

# debian/watch templates based on https://wiki.debian.org/debian/watch
# and https://wiki.debian.org/Python/LibraryStyleGuide

watch_templates = {
    r"github.com/(?P<user>[\w\-]*)/(?P<project>[\w\-]*)": r"""
opts=filenamemangle=s/.+\/v?(\d\S*)\.tar\.gz/{pkgname}-$1\.tar\.gz/ \
  https://{url}/tags .*/v?(\d\S*)\.tar\.gz
""",
    "metacpan.org": r"""
https://metacpan.org/release/{project} .*/{project}-v?(\d[\d.]+)\.(?:tar(?:\.gz|\.bz2)?|tgz|zip)$
""",
    r"pypi.python.org/pypi/(?P<project>[\w\.\-]*)": r"""
opts=uversionmangle=s/(rc|a|b|c)/~$1/ \
https://pypi.debian.net/{project}/{project}-(.+)\.(?:zip|tgz|tbz|txz|(?:tar\.(?:gz|bz2|xz)))
""",
    r"pypi.org/project/(?P<project>[\w\.\-]*)": r"""
opts=uversionmangle=s/(rc|a|b|c)/~$1/ \
http://pypi.debian.net/{project}/{project}-(.+)\.(?:zip|tgz|tbz|txz|(?:tar\.(?:gz|bz2|xz)))
""",
    r"code.google.com/p/(?P<project>[\w\-]*)": r"""
http://code.google.com/p/{project}/downloads/list?can=1 .*/{project}-(\d\S*)\.(?:zip|tgz|tbz|txz|(?:tar\.(?:gz|bz2|xz)))
""",
    r"bitbucket.org/(?P<user>[\w\-]*)/(?P<project>[\w\-]*)": r"""
https://bitbucket.org/{user}/{project}/downloads .*/(\d\S*)\.tar\.gz
""",
    "search.cpan.org": r"""
http://search.cpan.org/dist/{project}/   .*/{project}-v?(\d[\d.-]+)\.(?:tar(?:\.gz|\.bz2)?|tgz|zip)$
""",
    r"launchpad.net/(?P<project>[\w\-]*)": """
https://launchpad.net/{project}/+download .*/{project}-(.*).tar.gz
""",
    r"codingteam.net/project/(?P<project>[\w\-]*)": r"""
https://codingteam.net/project/{project}/download project/{project}/download/file/{project}-(\d\S*).tar.gz
    """,
    r"gitlab.com/(?P<user>[\w\-]*)/(?P<project>[\w\-]*)": r"""
https://gitlab.com/{user}/{project}/tags?sort=updated_desc archive/@ANY_VERSION@/{project}-\d\S*@ARCHIVE_EXT@
""",
    r"gitlab.gnome.org/(?P<user>[\w\-]*)/(?P<project>[\w\-]*)": r"""
https://gitlab.gnome.org/{user}/{project}/tags?sort=updated_desc archive/@ANY_VERSION@/{project}-\d\S*@ARCHIVE_EXT@
""",
    r"salsa.debian.org/(?P<user>[\w\-]*)/(?P<project>[\w\-]*)": r"""
https://salsa.debian.org/{user}/{project}/tags?sort=updated_desc archive/@ANY_VERSION@/{project}-\d\S*@ARCHIVE_EXT@
""",
}


def detect_hosting_service(url, pkg_name=None):
    """Detect well-known hosting service

    :param url: project URL
    :param pkg_name: optional package name
    :returns: proposed project name (str), watch file contents (str)
    """

    if not url.startswith(("http://", "https://")):
        raise Exception("Unknown protocol in URL")

    url = url.rstrip("/")
    url = url.split("/", 2)[-1]  # remove http[s]://

    for url_re in sorted(watch_templates):
        match = re.match(url_re, url)
        if match:
            break

    if not match:
        raise Exception("Unknown service")

    watch_tpl = watch_templates[url_re]

    d = match.groupdict()
    d["url"] = url
    d["project_first_letter"] = d["project"][0]
    d["pkgname"] = pkg_name or d["project"].lower()

    proposed_project_name = url.split("/")[-1]
    log.debug("Package name: %s", d["pkgname"])
    log.debug("Project full name: %s", proposed_project_name)
    log.debug("Project name: %s", proposed_project_name)
    watch = watch_tpl.format(**d)
    watch = "version=4\n%s" % watch

    return proposed_project_name, watch


def write_watch_file(contents):
    with open("debian/watch", "w") as f:
        f.write(contents)


def parse_args():
    ap = ArgumentParser(
        description=__doc__, formatter_class=RawDescriptionHelpFormatter
    )
    ap.add_argument("url")
    ap.add_argument("--pkg-name", help="Debian package name (default: inferred)")
    return ap.parse_args()


def main():
    args = parse_args()
    proposed_project_name, watch = detect_hosting_service(args.url, args.pkg_name)
    write_watch_file(watch)


if __name__ == "__main__":
    main()
